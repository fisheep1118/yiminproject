import sys
from PyQt5.QtChart import QDateTimeAxis, QValueAxis,QLineSeries, QSplineSeries, QChart, QChartView
from PyQt5.QtCore import QDateTime, Qt, QTimer
from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QApplication
# %%
class ChartView(QChartView, QChart):
    def __init__(self, yuzhi, *args, **kwargs):  # 调用父类构造函数
        super(ChartView, self).__init__(*args, **kwargs)
        self.resize(1000, 800)  # 设置ChartView的大小为800x600像素
        self.setRenderHint(QPainter.Antialiasing)  # 设置渲染提示为抗锯齿，使得绘制的图表看起来更平滑
        self.yint1 = 0
        # self.data_newpoint = pd.DataFrame(columns=['data_1', 'data_2', 'scores', 'outliers'])
        self.threshold = yuzhi  # 示例阈值
        self.chart_init()  # 初始化图表
        self.timer_init()  # 初始化定时器

    def timer_init(self):
#这个方法初始化一个 QTimer 对象实例。定时器设置为每隔3秒引发一次 self.drawLine 方法，从而定时更新数据。
        # 使用QTimer，2秒触发一次，更新数据
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.drawLine)     #定时器调用 这个方法
        self.timer.start(5000)
    def chart_init(self):
#这个方法初始化图表的外观和属性，包括曲线系列、坐标轴、阈值线、坐标轴范围、坐标轴标题及格式等。
        self.chart = QChart()
        # self.series = QSplineSeries()     #平滑线
        self.series = QLineSeries()
        self.threshold_line = QLineSeries()  # 新建阈值线系列
        self.series.setName("2号机主机#8、#9瓦X方向振动GMM得分")        # 设置线名称
        self.threshold_line.setName("阈值")           # 设置线名称
        self.chart.addSeries(self.series)            # 把线添加到QChart的实例中
        self.dtaxisX = QDateTimeAxis()               # 声明并初始化X轴
        self.vlaxisY = QValueAxis()                  # 声明并初始化Y轴
        self.dtaxisX.setMin(QDateTime.currentDateTime().addSecs(-60 * 1))   # 设置坐标轴显示范围X最小，60秒前
        self.dtaxisX.setMax(QDateTime.currentDateTime().addSecs(0))         # 设置坐标轴显示范围X最大，当前
        self.vlaxisY.setMin(0)         # 设置坐标轴显示范围Y最小
        self.vlaxisY.setMax(30)         # 设置坐标轴显示范围Y最大
        self.dtaxisX.setFormat("hh:mm:ss")   # 设置X轴时间样式
        self.dtaxisX.setTickCount(12)    # 设置X坐标轴上的格点
        self.vlaxisY.setTickCount(9)     # 设置Y坐标轴上的格点
        self.dtaxisX.setTitleText("时间（s）")    # 设置坐标轴名称
        self.vlaxisY.setTitleText("GMM得分")    # 设置坐标轴名称
        self.vlaxisY.setGridLineVisible(True)  # 设置网格显示
        self.chart.addAxis(self.dtaxisX, Qt.AlignBottom)   # 把X坐标轴添加到chart中
        self.chart.addAxis(self.vlaxisY, Qt.AlignLeft)     # 把Y坐标轴添加到chart中
        self.series.attachAxis(self.dtaxisX)               # 把曲线关联到坐标轴x
        self.series.attachAxis(self.vlaxisY)               # 把曲线关联到坐标轴y
        self.chart.addSeries(self.threshold_line)          # 把阈值线添加到QChart的实例中
        self.threshold_line.attachAxis(self.dtaxisX)       # 把阈值线关联到坐标轴
        self.threshold_line.attachAxis(self.vlaxisY)       # 把阈值线关联到坐标轴
        self.setChart(self.chart)
    def drawLine(self):
        bjtime = QDateTime.currentDateTime()               # 获取当前时间
        self.dtaxisX.setMin(QDateTime.currentDateTime().addSecs(-60 * 1))             # 更新X轴坐标
        self.dtaxisX.setMax(QDateTime.currentDateTime().addSecs(0))                   # 更新X轴坐标
        if (self.series.count() > 60):        # 当曲线上的点超出X轴的范围时，移除最早的点
            self.series.removePoints(0, self.series.count() - 60)
        self.series.append(bjtime.toMSecsSinceEpoch(), self.yint1)        # 生成新的时间戳并将结果附加到数据序列 self.series
        self.threshold_line.append(bjtime.toMSecsSinceEpoch(), self.threshold)
    def update_yint(self, value1):      #设置新的 yint 值，并立即触发 self.drawLine 方法以更新图表。
        self.yint1 = value1
        self.drawLine()           # 调用 drawLine 方法，将新值反映在图表上
    def save_data(self):
        view.data_newpoint.to_csv(filename='data_newpoint.csv', index=False)

#%%
if __name__ == "__main__":
    app = QApplication(sys.argv)
    view = ChartView()
    view.show()
    sys.exit(app.exec_())