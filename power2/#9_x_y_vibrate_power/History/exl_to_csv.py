#%%
import pandas as pd  # 处理和分析数据
import os
#%%
# 读取 Excel 文件 设置转换目录👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇
input_file = 'TagsHistory.xlsx'
# 读取 Excel 文件 设置转换目录👆👆👆👆👆👆👆👆👆👆👆👆👆👆👆👆👆👆👆
# 构建输出文件路径保持在相同目录下
output_file = os.path.dirname(input_file) + 'TagsHistory.csv'

if not os.path.exists(output_file):
    # 读取 Excel 文件
    df = pd.read_excel(input_file)
    # 将数据保存为 csv 文件
    output_file = os.path.dirname(input_file) + input_file.split('.')[0] + '.csv'
    df.to_csv(output_file, index=False, encoding='utf_8_sig')
    print('文件保存成功')
else:
    print('文件已存在，跳过保存操作')
#%%
df = pd.read_csv(output_file)
print(df.head(4))
#%%
# 删除所有包含 "Unnamed:" 的列名
# 创建替换字典
columns = df.columns
# 创建替换字典
replace_dict = {}
for i in range(1, len(columns)):
    if i % 4 == 1:  # 判断是否为4n+1位置的列名
        replace_dict[columns[i]] = columns[i-1]
        replace_dict[columns[i-1]] = columns[i]
# 替换列名
df = df.rename(columns=replace_dict)
#%%
df = df.loc[:, ~df.columns.str.contains('Unnamed')]
df = df.drop(df.index[0])
#%%
df = df.dropna()  #删除错误行
#%%
df.to_csv(output_file, index=False,encoding='utf_8_sig')
print('文件保存成功')