import pandas as pd  # 处理和分析数据
from sklearn.mixture import GaussianMixture  # 高斯混合模型
from matplotlib.font_manager import FontProperties  # 设置图表的字体属性


font_path = r"D:/project/Microsoft YaHei.ttc"
font = FontProperties(fname=font_path, size=14)
# %%
# 数据加载和处理
df = pd.read_csv('History/TagsHistory.csv')
X = df[df.columns].values
print(X)
# %%
gmm = GaussianMixture(n_components=5, random_state=0)  # 初始化高斯混合模型，设置成分数为1
gmm.fit(X)  # 拟合模型到数据集，我们需要将DataFrame转为Numpy数组

scores = gmm.score_samples(X)  # 计算每个样本点的得分per
GMMthreshold = scores.mean() - 3 * scores.std()  # 计算阈值
print('该模型的阈值为', GMMthreshold)
# %%
# 存储模型（如有必要）
from joblib import dump

dump(gmm, 'Gmm_model/gmm.joblib')
print('高斯混合模型gmm.joblib已保存到power2/#8_x_y_vibrate/Gmm_model/gmm1.joblib')
# 存储阈值
import numpy as np

np.save('Gmm_model/gmm_threshold.npy', GMMthreshold)  # 保存为.npy文件
print('高斯混合模型阈值gmm_threshold.npy已保存到power2/#8_x_y_vibrate/Gmm_model/gmm_threshold.npy')
# %%
