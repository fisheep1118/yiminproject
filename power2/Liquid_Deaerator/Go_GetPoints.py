from View_main import ChartView
import sys
import requests
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTimer
import numpy as np
from joblib import load   #加载GMM模型

gmm = load('.\Gmm_model\gmm.joblib')
GMM_threshold = np.load('.\Gmm_model\gmm_threshold.npy')
cedian = "N2DCS.RL507L"  # 更换不同的点值
host = ("http://10.41.41.161:8081/point/api?cedian=" + cedian)
print(gmm)
def get_new_data():
    try:
        response = requests.get(host)
        if response.status_code == 200:
            data = response.json()['data']
            data_point = np.array([[data["N2DCS.RL507L"]]])
            log_prob = gmm.score_samples(data_point)
            data['log_prob'] =log_prob
            if log_prob < GMM_threshold:
                print("异常值:", data_point)
            else:
                print("正常:", data_point)
            yint1 = abs(data['log_prob'])  # 使用变量 cedian
            print('GMM得分是',yint1)
            print('阈值是',GMM_threshold)
            view.update_yint(yint1)  # 调用函数”update_yint“
    except requests.RequestException as e:
        print(f"An error occurred while fetching data: {e}")
# 主执行区块：Python 程序执行的入口点。
if __name__ == "__main__":
    GO = QApplication(sys.argv)    # 创建一个应用实例并运行
    yuzhi = abs(GMM_threshold)    # 设置 chart 的示例阈值
    series_name = "#2机组除氧器水位"  # 添加这里的变量
    y_axis_min = 0  # 输入需要设置的最小Y值
    y_axis_max = 10  # 输入需要设置的最大Y值
    view = ChartView(yuzhi, series_name, y_axis_min, y_axis_max)  # 传入构造函数
    view.show()                     #执行view
    timer = QTimer()               # 创建定时器以实时更新代理的数据数据
    timer.timeout.connect(get_new_data) # 每隔一段时间（这里设置为3秒）触发一次数据更新
    timer.start(1000)
    # 应用退出前执行的操作
    # 运行应用程序
    sys.exit(GO.exec_())

# Python 解释器开始逐行执行代码。
# QApplication 实例启动，建立了程序的主循环。
# ChartView 实例创建并显示图表。
# 定时器启动，定时地触发 get_new_data 函数。
# get_new_data 从远程服务器请求数据，并将获取到的 yint 值传递给图表视图，从而更新图表。
# 当你关闭程序，触发 save_data() 函数，数据被保存到文件中。
# 这个过程反复进行，从而实现了持续的数据抓取和图表更新，在用户界面上生成动态的、实时的图表展示。